package uk.co.unclealex.days

import java.time.{Instant, ZoneId, ZonedDateTime}

trait ZonedDSL:

  given zoneId: ZoneId

  given Conversion[DateAndTime, ZonedDateTime] = _.toZonedDateTime

  given Conversion[DateAndTime, Instant] = _.toZonedDateTime.toInstant
