package uk.co.unclealex.days

/** Helper classes used to make dates and times more readable in tests.
  *
  * @author
  *   alex
  */
enum Month(val name: String, val month: Int):

  override def toString: String = name
  def apply(day: Int): MonthAndDay = MonthAndDay(day, this)
  def apply(day: Int, year: Int): Date = Date(apply(day), year)

  case January extends Month("January", 1)
  case February extends Month("February", 2)
  case March extends Month("March", 3)
  case April extends Month("April", 4)
  case May extends Month("May", 5)
  case June extends Month("June", 6)
  case July extends Month("July", 7)
  case August extends Month("August", 8)
  case September extends Month("September", 9)
  case October extends Month("October", 10)
  case November extends Month("November", 11)
  case December extends Month("December", 12)
