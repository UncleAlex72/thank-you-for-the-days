package uk.co.unclealex.days

trait Syntax extends DSL with DateAndTimeCanEquals

object Syntax extends Syntax
