package uk.co.unclealex.days

import java.time.LocalTime

case class Time(hours: Int, minutes: Int):

  def am: Time =
    if (hours >= 12)
      copy(hours = hours - 12)
    else
      this

  def pm: Time =
    if (hours < 12)
      copy(hours = hours + 12)
    else
      this

  def toLocalTime: LocalTime = LocalTime.of(hours, minutes)
