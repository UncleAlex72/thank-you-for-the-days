package uk.co.unclealex.days

case class MonthAndDay(day: Int, month: Month)
