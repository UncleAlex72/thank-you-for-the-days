package uk.co.unclealex.days

import java.time._

import org.scalactic.CanEqual

import Syntax.*

trait DateAndTimeCanEquals {

  // noinspection ConvertExpressionToSAM
  private def swap[A, B](canEqual: CanEqual[B, A]): CanEqual[A, B] =
    new CanEqual[A, B] {
      override def areEqual(a: A, b: B): Boolean = canEqual.areEqual(b, a)
    }

  implicit val dateAndTimeCanEqualZonedDateTime
      : CanEqual[DateAndTime, ZonedDateTime] =
    (a: DateAndTime, b: ZonedDateTime) => {
      a.toZonedDateTime(using b.getZone) == b
    }

  implicit val zonedDateTimeCanEqualDateAndTime
      : CanEqual[ZonedDateTime, DateAndTime] =
    swap(dateAndTimeCanEqualZonedDateTime)

  implicit def dateAndTimeCanEqualInstantWithZoneId(implicit
      zoneId: ZoneId
  ): CanEqual[DateAndTime, Instant] =
    (a: DateAndTime, b: Instant) => {
      a.toInstant == b
    }

  implicit def instantWithZoneIdCanEqualDateAndTime(implicit
      zoneId: ZoneId
  ): CanEqual[Instant, DateAndTime] =
    swap(dateAndTimeCanEqualInstantWithZoneId)

  implicit val dateCanEqualLocalDate: CanEqual[Date, LocalDate] =
    (a: Date, b: LocalDate) => a.toLocalDate == b

  implicit val localDateCanEqualDate: CanEqual[LocalDate, Date] = swap(
    dateCanEqualLocalDate
  )

  implicit val timeCanEqualLocalTime: CanEqual[Time, LocalTime] =
    (a: Time, b: LocalTime) => a.toLocalTime == b

  implicit val localTimeCanEqualTime: CanEqual[LocalTime, Time] = swap(
    timeCanEqualLocalTime
  )

  implicit val dateAndTimeCanEqualLocalDateTime
      : CanEqual[DateAndTime, LocalDateTime] =
    (a: DateAndTime, b: LocalDateTime) => a.toLocalDateTime == b

  implicit val localDateTimeCanEqualDateAndTime
      : CanEqual[LocalDateTime, DateAndTime] = swap(
    dateAndTimeCanEqualLocalDateTime
  )
}

object DateAndTimeCanEquals extends DateAndTimeCanEquals
