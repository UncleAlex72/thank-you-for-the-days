package uk.co.unclealex.days

import java.time.temporal.Temporal
import java.time.temporal.ChronoField.*

import java.time.{
  Instant,
  LocalDateTime,
  OffsetDateTime,
  ZoneId,
  ZoneOffset,
  ZonedDateTime
}

case class DateAndTime(date: Date, time: Time):

  override def toString: String = f"$date ${time.hours}%02d:${time.minutes}%02d"

  def toLocalDateTime: LocalDateTime =
    LocalDateTime.of(date.toLocalDate, time.toLocalTime)

  def toZonedDateTime(using zoneId: ZoneId): ZonedDateTime =
    toLocalDateTime.atZone(zoneId)

  def toOffsetDateTime(using zoneOffset: ZoneOffset): OffsetDateTime =
    toLocalDateTime.atOffset(zoneOffset)

  def toInstant(using zoneId: ZoneId): Instant =
    toZonedDateTime.toInstant

object DateAndTime:

  def apply(temporal: Temporal): DateAndTime =
    Month.values.find(month => month.month == temporal.get(MONTH_OF_YEAR)) match
      case Some(month) =>
        DateAndTime(
          Date(
            MonthAndDay(temporal.get(DAY_OF_MONTH), month),
            temporal.get(YEAR)
          ),
          Time(temporal.get(HOUR_OF_DAY), temporal.get(MINUTE_OF_HOUR))
        )
      case None =>
        throw new IllegalArgumentException(
          s"Cannot find a month for $temporal"
        )
