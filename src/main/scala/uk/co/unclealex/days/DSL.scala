package uk.co.unclealex.days

import java.time.{Instant, LocalDate, LocalDateTime, ZoneId, ZonedDateTime}

trait DSL:

  val January: Month = Month.January
  val February: Month = Month.February
  val March: Month = Month.March
  val April: Month = Month.April
  val May: Month = Month.May
  val June: Month = Month.June
  val July: Month = Month.July
  val August: Month = Month.August
  val September: Month = Month.September
  val October: Month = Month.October
  val November: Month = Month.November
  val December: Month = Month.December

  given Conversion[DateAndTime, LocalDateTime] = _.toLocalDateTime

  given Conversion[Date, LocalDate] = _.toLocalDate

  extension (hour: Int)

    def am: Time = Time(hour, 0).am
    def pm: Time = Time(hour, 0).pm
    def asIs: Time = Time(hour, 0)

    private def withMinutes(minutes: Int): Time = Time(hour, minutes)

    // 00
    def _00: Time = withMinutes(0)
    def _01: Time = withMinutes(1)
    def _02: Time = withMinutes(2)
    def _03: Time = withMinutes(3)
    def _04: Time = withMinutes(4)
    def _05: Time = withMinutes(5)
    def _06: Time = withMinutes(6)
    def _07: Time = withMinutes(7)
    def _08: Time = withMinutes(8)
    def _09: Time = withMinutes(9)

    // 10
    def _10: Time = withMinutes(10)
    def _11: Time = withMinutes(11)
    def _12: Time = withMinutes(12)
    def _13: Time = withMinutes(13)
    def _14: Time = withMinutes(14)
    def _15: Time = withMinutes(15)
    def _16: Time = withMinutes(16)
    def _17: Time = withMinutes(17)
    def _18: Time = withMinutes(18)
    def _19: Time = withMinutes(19)

    // 20
    def _20: Time = withMinutes(20)
    def _21: Time = withMinutes(21)
    def _22: Time = withMinutes(22)
    def _23: Time = withMinutes(23)
    def _24: Time = withMinutes(24)
    def _25: Time = withMinutes(25)
    def _26: Time = withMinutes(26)
    def _27: Time = withMinutes(27)
    def _28: Time = withMinutes(28)
    def _29: Time = withMinutes(29)

    // 30
    def _30: Time = withMinutes(30)
    def _31: Time = withMinutes(31)
    def _32: Time = withMinutes(32)
    def _33: Time = withMinutes(33)
    def _34: Time = withMinutes(34)
    def _35: Time = withMinutes(35)
    def _36: Time = withMinutes(36)
    def _37: Time = withMinutes(37)
    def _38: Time = withMinutes(38)
    def _39: Time = withMinutes(39)

    // 40
    def _40: Time = withMinutes(40)
    def _41: Time = withMinutes(41)
    def _42: Time = withMinutes(42)
    def _43: Time = withMinutes(43)
    def _44: Time = withMinutes(44)
    def _45: Time = withMinutes(45)
    def _46: Time = withMinutes(46)
    def _47: Time = withMinutes(47)
    def _48: Time = withMinutes(48)
    def _49: Time = withMinutes(49)

    // 50
    def _50: Time = withMinutes(50)
    def _51: Time = withMinutes(51)
    def _52: Time = withMinutes(52)
    def _53: Time = withMinutes(53)
    def _54: Time = withMinutes(54)
    def _55: Time = withMinutes(55)
    def _56: Time = withMinutes(56)
    def _57: Time = withMinutes(57)
    def _58: Time = withMinutes(58)
    def _59: Time = withMinutes(59)

object DSL extends DSL
