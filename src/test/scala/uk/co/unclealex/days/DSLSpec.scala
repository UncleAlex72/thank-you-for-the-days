package uk.co.unclealex.days

import java.time.{ZonedDateTime, *}
import java.time.format.DateTimeFormatter
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class DSLSpec extends AnyWordSpec with Matchers with DSL with ZonedDSL {

  given zoneId: ZoneId = ZoneId.of("Europe/London")

  val dateTimeFormatter: DateTimeFormatter =
    DateTimeFormatter.ISO_ZONED_DATE_TIME

  "Adding minutes to an hour" should {
    Range(0, 59).inclusive
      .zip(
        Seq(
          10._00,
          10._01,
          10._02,
          10._03,
          10._04,
          10._05,
          10._06,
          10._07,
          10._08,
          10._09,
          10._10,
          10._11,
          10._12,
          10._13,
          10._14,
          10._15,
          10._16,
          10._17,
          10._18,
          10._19,
          10._20,
          10._21,
          10._22,
          10._23,
          10._24,
          10._25,
          10._26,
          10._27,
          10._28,
          10._29,
          10._30,
          10._31,
          10._32,
          10._33,
          10._34,
          10._35,
          10._36,
          10._37,
          10._38,
          10._39,
          10._40,
          10._41,
          10._42,
          10._43,
          10._44,
          10._45,
          10._46,
          10._47,
          10._48,
          10._49,
          10._50,
          10._51,
          10._52,
          10._53,
          10._54,
          10._55,
          10._56,
          10._57,
          10._58,
          10._59
        )
      )
      .foreach { case (minutes, time) =>
        f"add $minutes minute${if (minutes == 1) "" else "s"} for _$minutes%02d" in {
          time should ===(Time(10, minutes))
        }
      }
  }

  "My time of birth" should {
    "resolve to September 5th 1972 at 9.12am" in {
      (September(5, 1972) at 9._12.am)
        .toZonedDateTime(using ZoneId.of("Europe/London")) should ===(
        ZonedDateTime.parse("1972-09-05T09:12:00+01:00[Europe/London]")
      )
    }
  }

  "Converting to AM" should {
    "convert 12am to 0 hours" in {
      12.am should ===(Time(0, 0))
      12._07.am should ===(Time(0, 7))
    }
    "convert 15am to 3 hours" in {
      15.am should ===(Time(3, 0))
      15._07.am should ===(Time(3, 7))
    }
    "convert 5am to 5 hours" in {
      5.am should ===(Time(5, 0))
      5._07.am should ===(Time(5, 7))
    }
  }

  "Converting to PM" should {
    "convert 12pm to 12 hours" in {
      12.pm should ===(Time(12, 0))
      12._07.pm should ===(Time(12, 7))
    }
    "convert 15pm to 15 hours" in {
      15.pm should ===(Time(15, 0))
      15._07.pm should ===(Time(15, 7))
    }
    "convert 5am to 17 hours" in {
      5.pm should ===(Time(17, 0))
      5._07.pm should ===(Time(17, 7))
    }
  }

  "Working with time zone IDs" should {
    implicit val zoneId: ZoneId = ZoneId.of("Europe/London")
    "convert a DateTime to a ZonedDateTime correctly" in {
      val actual: ZonedDateTime = September(5, 1972) at 9._12.am
      actual should ===(
        ZonedDateTime.parse("1972-09-05T09:12:00+01:00[Europe/London]")
      )
    }
    "convert a DateTime to an Instant correctly" in {
      val actual: Instant = September(5, 1972) at 9._12.am
      actual should ===(Instant.ofEpochSecond(84528720))
    }
  }
}
