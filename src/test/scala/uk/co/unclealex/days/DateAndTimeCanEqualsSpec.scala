package uk.co.unclealex.days

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import java.time.*

class DateAndTimeCanEqualsSpec extends AnyWordSpec with Matchers with Syntax {

  implicit val timeZone: ZoneId = ZoneId.of("Europe/London")

  "Equality" should {
    "correctly match a zonedDateTime to a dateAndTime" in {
      ZonedDateTime.parse(
        "1972-09-05T09:12:00+01:00[Europe/London]"
      ) should ===(September(5, 1972) at 9._12.am)
    }
    "correctly not match a zonedDateTime to a dateAndTime" in {
      ZonedDateTime.parse(
        "1972-09-05T09:12:00+01:00[Europe/London]"
      ) should !==(September(5, 1972) at 9._15.am)
    }
    "correctly match a dateAndTime to a zonedDateTime" in {
      September(5, 1972) at 9._12.am should ===(
        ZonedDateTime.parse("1972-09-05T09:12:00+01:00[Europe/London]")
      )
    }
    "correctly not match a dateAndTime to a zonedDateTime" in {
      September(5, 1972) at 9._15.am should !==(
        ZonedDateTime.parse("1972-09-05T09:12:00+01:00[Europe/London]")
      )
    }
    "correctly match a dateTime to an instant" in {
      September(5, 1972) at 9._12.am should ===(Instant.ofEpochSecond(84528720))
    }
    "correctly not match a dateTime to an instant" in {
      September(5, 1972) at 9._15.am should !==(Instant.ofEpochSecond(84528720))
    }
    "correctly match an instant to a dateAndTime" in {
      Instant.ofEpochSecond(84528720) should ===(September(5, 1972) at 9._12.am)
    }
    "correctly not match an instant to a dateAndTime" in {
      Instant.ofEpochSecond(84528720) should !==(September(5, 1972) at 9._15.am)
    }
    "correctly match a date to a localDate" in {
      September(5, 1972) should ===(LocalDate.of(1972, 9, 5))
    }
    "correctly not match a date to a localDate" in {
      September(6, 1972) should !==(LocalDate.of(1972, 9, 5))
    }
    "correctly match a localDate to a date" in {
      LocalDate.of(1972, 9, 5) should ===(September(5, 1972))
    }
    "correctly not match a localDate to a date" in {
      LocalDate.of(1972, 9, 6) should !==(September(5, 1972))
    }
    "correctly match a time to a localTime" in {
      9._12 should ===(LocalTime.of(9, 12))
    }
    "correctly not match a time to a localTime" in {
      9._12 should !==(LocalTime.of(9, 15))
    }
    "correctly match a localTime to a time" in {
      LocalTime.of(9, 12) should ===(9._12)
    }
    "correctly not match a localTime to a time" in {
      LocalTime.of(9, 15) should !==(9._12)
    }
  }

}
