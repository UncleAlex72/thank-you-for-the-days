import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._
import sbt.Keys._
import sbt._

val _scalaVersion = "3.5.0"

//resolvers += "bintray/non" at "http://dl.bintray.com/non/maven"

lazy val days = (project in file(".")).settings(
  name := "thank-you-for-the-days",
  organization := "uk.co.unclealex",
  libraryDependencies ++= Seq(
    "org.scalatest" %% "scalatest" % "3.2.19",
    "org.scalactic" %% "scalactic" % "3.2.19",
    "ch.qos.logback" % "logback-classic" % "1.2.3" % Test),
  scalaVersion := _scalaVersion
)

resolvers ++= Seq(
  "Atlassian Releases" at "https://maven.atlassian.com/public/",
  Resolver.jcenterRepo)

scalacOptions in Test ++= Seq("-Yrangepos")

organizationHomepage := Some(url("https://bitbucket.org/UncleAlex72/"))

scmInfo := Some(
  ScmInfo(
    url("https://bitbucket.org/UncleAlex72/thank-you-for-the-days"),
    "scm:git@bitbucket.org:UncleAlex72/thank-you-for-the-days.git"
  )
)
developers := List(
  Developer(
    id = "1",
    name = "Alex Jones",
    email = "alex.jones@unclealex.co.uk",
    url = url("https://bitbucket.org/UncleAlex72/")
  )
)

description := "A useful DSL for testing with dates."
licenses := List(
  "Apache 2" -> new URL("http://www.apache.org/licenses/LICENSE-2.0.txt")
)
homepage := Some(url("https://bitbucket.org/UncleAlex72/thank-you-for-the-days"))

// Remove all additional repository other than Maven Central from POM
pomIncludeRepository := { _ =>
  false
}

publishTo := sonatypePublishToBundle.value

publishMavenStyle := true

releaseProcess := Seq[ReleaseStep](
  releaseStepCommand("scalafmtCheckAll"),
  checkSnapshotDependencies, // : ReleaseStep
  inquireVersions, // : ReleaseStep
  runTest, // : ReleaseStep
  setReleaseVersion, // : ReleaseStep
  commitReleaseVersion, // : ReleaseStep, performs the initial git checks
  tagRelease, // : ReleaseStep
  releaseStepCommand("publishLocal"),
  releaseStepCommand("publishSigned"),
  releaseStepCommand("sonatypeBundleRelease"),
  setNextVersion, // : ReleaseStep
  commitNextVersion, // : ReleaseStep
  pushChanges // : ReleaseStep, also checks that an upstream branch is properly configured
)
